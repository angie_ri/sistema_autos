<!doctype html>
<html lang="es">
<?php
include("vistas/head.php");
include_once('modelos/Auto.php');
include_once('modelos/Ventas.php');
//buscar autos
$auto = new Auto("sistema_autos");
$autos = $auto->conseguirAuto();

$venta = new Ventas("sistema_autos");
$ventas = $venta->conseguirVenta();

?>

<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">

</div>
<?php
if(isset($_GET['id']) && $_GET['id']!= "autos" &&$_GET['id']!= "ventas" ) {

    ?>
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Error!</h1>
        <h2>No se encontro esa página</h2>
    </div>

    <?php
}

if(isset($_GET['estado']))
{
    if($_GET['estado'] == "true") {


        echo '<div class="alert alert-success w-50 text-center m-auto" role="alert" >
						  Registro eliminado!
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
    }else{
        echo '<div class="alert alert-danger w-50 text-center m-auto" role="alert" >
						  No se puede eliminar el registro.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
    }
}

if(isset($_GET['id']) && $_GET['id']== "autos") {

?>
<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Lista de Autos</h1>
</div>

<div class="container verlista">


    <table class="table table-bordered display" id="table_cli" data-page-length='5'>
        <thead>
        <tr>
            <th scope="col">Auto</th>
            <th scope="col">Modelo</th>
            <th scope="col">Marca</th>
            <th scope="col">Distribuidor</th>
            <th scope="col">Origen</th>
            <th scope="col">Precio</th>
            <th scope="col">Usuario</th>
            <th scope="col">Fecha de Ingreso</th>
            <th scope="col">Acciones</th>
        </tr>
        </thead>
        <tbody>

                <?php

                foreach($autos as $auto){

                    echo '<tr>
                    <th scope="row">'. $auto['nombre'] .'</th>
                    <td>'. $auto["modelo"] .'</td>
                    <td>'. $auto["marca"].'</td>
                    <td>'. $auto["distribuidor"].'</td>
                    <td>'. $auto["origen"].'</td>
                    <td>'. $auto["precio"].'</td>
                    <td>'. $auto["usuario"].'</td>
                    <td>'. $auto["fecha_ingreso"].'</td>
                    <td>
                    <a href="accion.php?id='. $auto['id'] .'&tipo=auto" ><button type="button" class="btn btn-outline-dark btn-sm">Editar</button></a>
                    <a href="acciones.php?id='. $auto['id'] .'&tipo=borrarA" ><button type="button" class="btn btn-outline-dark btn-sm mt-1">Borrar</button></a>
                    </td>
                  </tr>';
                }

        ?>

        </tbody>
    </table>


    <div class="links">
        <a href="index.php" class="font-italic text-decoration-none text-left"><- Registrar Auto</a>
        <a href="listado.php?id=ventas" class="font-italic text-decoration-none text-right" id="linkVentas">Ver Ventas -></a>
    </div>


    <?php
    }
    if(isset($_GET['id']) && $_GET['id']== "ventas") {

?>
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Lista Ventas</h1>
    </div>

    <div class="container verlista" >



        <table class="table table-bordered display" id="table_cli" data-page-length='5'>
            <thead>
            <tr>
                <th scope="col">Auto</th>
                <th scope="col">Modelo</th>
                <th scope="col">Vendedor</th>
                <th scope="col">Sucursal</th>
                <th scope="col">Cliente</th>
                <th scope="col">Nro Cuotas</th>
                <th scope="col">Fecha Venta</th>
                <th scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>

                    <?php

                    foreach($ventas as $venta){

                        echo '<tr>
                        <th scope="row">'. $venta['id_auto'] .'</th>
                        <td>'. $venta["id_auto"] .'</td>
                        <td>'. $venta["vendedor"].'</td>
                        <td>'. $venta["local"].'</td>
                        <td>'. $venta["cliente"].'</td>
                        <td>'. $venta["cantidad_cuotas"].'</td>
                        <td>'. $venta["fecha_venta"].'</td>
                        <td><a href="accion.php?id='. $venta['id'] .'&tipo=venta" ><button type="button" class="btn btn-outline-dark btn-sm">Editar</button></a>
                            <a href="acciones.php?id='.$venta['id'] .'&tipo=borrarV" ><button type="button" class="btn btn-outline-dark btn-sm ">Borrar</button></a>
                            </td>
                      </tr>';
                    }

            ?>

            </tbody>
        </table>

        <div class="links">
            <a href="listado.php?id=autos" class="font-italic text-decoration-none text-left"><- Lista de Autos</a>
            <a href="ventas.php" class="font-italic text-decoration-none text-right" id="linkVentas">Registrar Venta -></a>
        </div>
    <?php
        }
    include("vistas/footer.php");
    ?>

</div>

<?php
include("vistas/scripts.php");
?>
</body>
</html>