
<!doctype html>
<html lang="en">
  <?php
     include("vistas/head.php");
     include("modelos/Auto.php");
     include("modelos/Ventas.php");

     $auto=null;
     $ventaE = null;
     $type=null;
  if(isset($_GET['tipo']))
  {
      $type = $_GET['tipo'];
      $id = $_GET['id'];
      if($type == "auto")
      {
          $auto =new Auto('sistema_autos');
          $autoE = $auto->query("Select * from autos where id = $id");

      }
      if($type =="venta"){
          $venta = new Ventas('sistema_autos');
          $ventaE = $venta->query("Select * from ventas where id = $id");

          $auto = new Auto("sistema_autos");

      }
  }

?>

  <body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">

    </div>
    <div class="container">
<?php
        if($type == 'auto')
    {
    ?>
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Auto</h1>
    </div>


        <?php


        foreach ($autoE as $valor) {

            echo
                '<form method="Post" action="acciones.php?id=' . $id . '">
        		     	<input type="hidden" name="form" value="autoEdit">
        		     	<input type="hidden" name="id" value="'.$valor["id"].'">
        		     	<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Nombre</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["nombre"] . '"  name="nombre">
        				</div>
        
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Modelo</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["modelo"] . '" name="modelo">
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Marca</span>
        				  </div>
        				  <input type="text" class="form-control" aria-describedby="basic-addon1" value="' . $valor["marca"] . '" name="marca">
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Administrador</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["usuario"] . '" name="usuario">
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Distribuidor</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["distribuidor"] . '" name="distribuidor">
        				</div>
        
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Origen</span>
        				  </div>
        				  <input type="text" class="form-control" aria-describedby="basic-addon1" value="' . $valor["origen"] . '" name="origen">
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Precio</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["precio"] . '" name="precio">
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Fecha de Ingreso</span>
        				  </div>
        				   
                           <input class="form-control" type="date"  id="example-date-input" value="' .$valor["fecha_ingreso"]. '" name="fecha_ingreso" >
                           
        				</div>
        				<button type="submit" class="btn btn-outline-dark btn-block">Enviar</button>
        	        </form>';

        }

        ?>


        <div class="links">
            <a href="listado.php?id=autos" class="font-italic text-decoration-none text-left"><- Lista de Autos</a>
            <a href="listado.php?id=ventas" class="font-italic text-decoration-none text-right" id="linkVentas">Ver Ventas -></a>
        </div>



        <?php
        }
        if($type == "venta")
        { ?>
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h1 class="display-4">Venta</h1>
        </div>


            <?php

            foreach ($ventaE as $valor) {

                $idA =(int) $valor["id_auto"];
                $autoE= $auto->query("Select * from autos where id = $idA");
//                $datosA = (array)$auto->query("Select * from autos where id = $idA");
                $nombre =null;
                $modelo=null;
                $marca=null;
                $precio=null;

                foreach ($autoE as $auto){

                    $nombre =$auto["nombre"];
                    $modelo=$auto["modelo"];
                    $marca=$auto["marca"];
                    $precio = $auto["precio"];
                }

                echo
                    '<form method="Post" action="acciones.php">
        		     	<input type="hidden" name="form" value="ventaEdit">
        		     	<input type="hidden" name="id" value="'.$valor['id'].'">
        		     	<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Auto</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $nombre . '"  disabled>
        				</div>
        
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Modelo</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $modelo . '" disabled>
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Marca</span>
        				  </div>
        				  <input type="text" class="form-control" aria-describedby="basic-addon1" value="' . $marca . '" disabled>
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Vendedor</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["vendedor"] . '" name="vendedor">
        				</div>
        
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Local</span>
        				  </div>
        				  <input type="text" class="form-control" aria-describedby="basic-addon1" value="' . $valor["local"] . '" name="local">
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Cliente</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["cliente"] . '" name="cliente">
        				</div>
        				<div class="input-group mb-3">
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">DNI Cliente</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["dni_cliente"] . '" name="dni_cliente">
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Precio</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $precio . '" disabled>
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Nro Cuotas</span>
        				  </div>
        				  <input type="text" class="form-control"  aria-describedby="basic-addon1" value="' . $valor["cantidad_cuotas"] . '" name="cantidad_cuotas">
        				</div>
        				<div class="input-group mb-3">
        				  <div class="input-group-prepend">
        				    <span class="input-group-text" id="basic-addon1">Fecha de Ingreso</span>
        				  </div>
        				  <input type="datetime-local" class="form-control" aria-describedby="basic-addon1" value="' . $valor["fecha_venta"] . '" name="fecha_ingreso" disabled>
        				</div>
        				<button type="submit" class="btn btn-outline-dark btn-block">Enviar</button>
        	        </form>';

            }

            ?>

            <div class="links">
                <a href="listado.php?id=autos" class="font-italic text-decoration-none text-left"><- Lista de Autos</a>
                <a href="listado.php?id=ventas" class="font-italic text-decoration-none text-right " style="margin-left: 15em" id="linkVentas">Ver Ventas -></a>
            </div>
    </div>
        <?php
        }
     include("vistas/footer.php");

      ?>
    </div>
   <?php
     include("vistas/scripts.php");
      ?>

  </body>
</html>