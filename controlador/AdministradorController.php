<?php

include_once('modelos/Auto.php');
include_once('modelos/Ventas.php');

class AdministradorController{

    private $accion;


    function __construct($accion)
    {
        $this->accion = $accion;
    }

    public function registrar($valores)
    {
        $estado = 0;
        if( $this->accion == "registrarA"){

            $auto =new Auto('sistema_autos');
            $estado =$auto->guardarAuto($valores);
            if($estado == 1)
            {
                return  header("Location:index.php");
            }
        }
        if( $this->accion == "registrarV"){

            $auto =new Ventas('sistema_autos');
            $estado =$auto->guardarVentas($valores);
            if($estado == 1)
            {
                return  header("Location:ventas.php");
            }
        }

    }

    public function modificar($valores,$valor,$columna)
    {
        if($this->accion =="autoEdit")
        {
            $auto =new Auto('sistema_autos');
            $estado = $auto->actualizarAuto($valores,$columna,$valor);

            if($estado == 1)
            {
                return  header("Location:listado.php?id=autos");
            }
        }
        if($this->accion == "ventaEdit") {
            $venta = new Ventas('sistema_autos');
            $estado = $venta->actualizarVentas($valores, $valor, $columna);

            if ($estado == 1) {
                return header("Location:listado.php?id=ventas");
            }
        }
    }


    public  function  eliminarRegistro($columna,$valor)
    {

        if($this->accion == "borrarA")
        {
            $auto = new Auto('sistema_autos');
            $estado = $auto->eliminarAuto($columna, $valor);
            if($estado == 1)
            {
                header("Location: listado.php?id=autos&estado=true");
            }else{
                header("Location: listado.php?id=autos&estado=false");
            }
        }
        if($this->accion == "borrarV")
        {
            $venta = new Ventas('sistema_autos');
            $estado = $venta->eliminarVenta($columna, $valor);
            if($estado == 1)
            {
                header("Location: listado.php?id=ventas&estado=true");
            }else{
                header("Location: listado.php?id=ventas&estado=false");
            }
        }

    }

    public  function  conseguirTodos()
    {
//        $auto = new Auto('sistema_autos');
//        $autos = $auto->conseguirAuto();
//        return $autos;
    }
    public function conseguir($id,$tabla)
    {

    }
}

?>