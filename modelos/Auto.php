<?php
include_once('clases/MyQuery.php');
include_once('clases/ConexionDB.php');
/*Clase persona*/
class Auto extends MyQuery {

        private $id ;
        private $modelo ;
        private $marca;
		private $nombre ;
        private $origen;
		private $usuario;
		private $precio;
		private $distribuidor;
		private $fecha_ingreso;
        private $fecha_actualizacion ;

        protected  $db;

        function __construct($base){

            $this->db = new ConexionDB($base);
        }



        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @param mixed $id
         */
        public function setId($id)
        {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getModelo()
        {
            return $this->modelo;
        }

        /**
         * @param mixed $modelo
         */
        public function setModelo($modelo)
        {
            $this->modelo = $modelo;
        }

        /**
         * @return mixed
         */
        public function getMarca()
        {
            return $this->marca;
        }

        /**
         * @param mixed $marca
         */
        public function setMarca($marca)
        {
            $this->marca = $marca;
        }

        /**
         * @return mixed
         */
        public function getOrigen()
        {
            return $this->origen;
        }

        /**
         * @param mixed $origen
         */
        public function setOrigen($origen)
        {
            $this->origen = $origen;
        }

        /**
         * @return mixed
         */
        public function getIdUser()
        {
            return $this->id_user;
        }

        /**
         * @param mixed $id_user
         */
        public function setIdUser($id_user)
        {
            $this->id_user = $id_user;
        }

        /**
         * @return mixed
         */
        public function getPrecio()
        {
            return $this->precio;
        }

        /**
         * @param mixed $precio
         */
        public function setPrecio($precio)
        {
            $this->precio = $precio;
        }

        /**
         * @return mixed
         */
        public function getDistribuidor()
        {
            return $this->distribuidor;
        }

        /**
         * @param mixed $distribuidor
         */
        public function setDistribuidor($distribuidor)
        {
            $this->distribuidor = $distribuidor;
        }

        /**
         * @return mixed
         */
        public function getFechaIngreso()
        {
            return $this->fecha_ingreso;
        }

        /**
         * @param mixed $fecha_ingreso
         */
        public function setFechaIngreso($fecha_ingreso)
        {
            $this->fecha_ingreso = $fecha_ingreso;
        }

        /**
         * @return mixed
         */
        public function getFechaActualizacion()
        {
            return $this->fecha_actualizacion;
        }

        /**
         * @param mixed $fecha_actualizacion
         */
        public function setFechaActualizacion($fecha_actualizacion)
        {
            $this->fecha_actualizacion = $fecha_actualizacion;
        }


//		public function guardarAuto($nombre,$modelo,$marca,$id_user,$distribuidor,$origen,$fecha_ingreso)
		public function guardarAuto($valores)
        {
            foreach ($valores as $key=>$value)
            {

                switch ($key)
                {
                    case 'nombre':
                        $this->nombre = $value;
                    break;
                    case 'modelo':
                        $this->modelo =$value;
                        break;
                    case 'marca':
                        $this->marca=$value;
                        break;
                    case 'usuario':
                        $this->usuario =$value;
                        break;
                    case 'distribuidor':
                        $this->distribuidor =$value;
                        break;
                    case 'origen':
                        $this->origen=$value;
                        break;
                    case 'precio':
                        $this->precio=$value;
                        break;
                }
            }
            $hoy = date("Y-m-d H:i:s");


            $valores = array("usuario" => $this->usuario, "fecha_actualizacion" => $hoy,
                "nombre" => $this->nombre, "modelo" => $this->modelo, "marca" => $this->marca, "distribuidor" => $this->distribuidor,
                "fecha_ingreso" => $hoy, "origen" => $this->origen,'precio'=>$this->precio);

            try {
                $sql = $this->insertarQuery("autos", $valores);
                $this->db->conectarDB();
                $this->db->ejecutar($sql);
                $this->db->cerrarConexion();

            } catch (ErrorException $e) {
                echo $e;
            }
            return true;
        }

        /**
         * $valores,$columna,$valor son variables que recibe para poder actualizar los datos del cliente
         */
        public function actualizarAuto($valores,$columna,$valor)
        {
            $sql = $this->updateQuery("autos",$valores,$columna,$valor);
            try{
                $this->db->conectarDB();
                $this->db->ejecutar($sql);
                $this->db->cerrarConexion();
            }catch (ErrorException $e){
                echo  $e;
            }
            return true;
		}


        /**
         * $valores,$columna,$valor son variables que recibe para poder actualizar los datos del cliente
         */
        public function eliminarAuto($columna,$valor)
        {
            $sql = $this->eliminarQuery("autos",$columna,$valor);
            try{
                $this->db->conectarDB();
                $this->db->ejecutar($sql);
                $this->db->cerrarConexion();

            }catch (ErrorException $e){
                echo  $e;
            }
            return true;
        }

        public function conseguirAuto(){

            $sql=$this->selectAllQuery("autos");

            $this->db->conectarDB();
            $autos= $this->db->consultar($sql);

            $this->db->cerrarConexion();
            return $autos;
        }


        /*
         *
         */
        public function query($sql){

            $this->db->conectarDB();
            $auto = $this->db->consultar($sql);
            $this->db->cerrarConexion();

            return $auto;
        }

	}
?>