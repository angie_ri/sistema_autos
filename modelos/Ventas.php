<?php


class Ventas extends MyQuery {

    private $id ;
    private $vendedor;
    private $id_auto;
    private $local;
    private $cliente;
    private $dni_cliente;
    private  $fecha_venta;
    private $cantidad_cuotas;

    protected  $db;

    function __construct($base){

        $this->db = new ConexionDB($base);
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor)
    {
        $this->vendedor = $vendedor;
    }

    /**
     * @return mixed
     */
    public function getIdAuto()
    {
        return $this->id_auto;
    }

    /**
     * @param mixed $id_auto
     */
    public function setIdAuto($id_auto)
    {
        $this->id_auto = $id_auto;
    }

    /**
     * @return mixed
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * @param mixed $local
     */
    public function setLocal($local)
    {
        $this->local = $local;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getDniCliente()
    {
        return $this->dni_cliente;
    }

    /**
     * @param mixed $dni_cliente
     */
    public function setDniCliente($dni_cliente)
    {
        $this->dni_cliente = $dni_cliente;
    }

    /**
     * @return mixed
     */
    public function getFechaVenta()
    {
        return $this->fecha_venta;
    }

    /**
     * @param mixed $fecha_venta
     */
    public function setFechaVenta($fecha_venta)
    {
        $this->fecha_venta = $fecha_venta;
    }

    /**
     * @return mixed
     */
    public function getCantidadCuotas()
    {
        return $this->cantidad_cuotas;
    }

    /**
     * @param mixed $cantidad_cuotas
     */
    public function setCantidadCuotas($cantidad_cuotas)
    {
        $this->cantidad_cuotas = $cantidad_cuotas;
    }

    public function guardarVentas($valores)
    {
        $hoy = date("Y-m-d H:i:s");

        foreach ($valores as $key=>$value)
        {

            switch ($key) {
                case 'vendedor':
                    $this->vendedor = $value;
                    break;
                case 'fecha_venta':
                    $this->fecha_venta = $hoy;
                    break;
                case 'id_auto':
                    $this->id_auto = $value;
                    break;
                case 'local':
                    $this->local = $value;
                    break;
                case 'cliente':
                    $this->cliente = $value;
                    break;
                case 'dni_cliente':
                    $this->dni_cliente = $value;
                    break;
                case 'cantidad_cuotas':
                    $this->cantidad_cuotas = $value;

            }
        }



        $valores = array("vendedor" => $this->vendedor, "fecha_venta" => $hoy,
            "id_auto" => $this->id_auto, "local" => $this->local, "cliente" => $this->cliente, "dni_cliente" => $this->dni_cliente,
            "cantidad_cuotas" => $this->cantidad_cuotas);

        try {
            $sql = $this->insertarQuery("ventas", $valores);
            echo $sql;
            $this->db->conectarDB();
            $this->db->ejecutar($sql);
            $this->db->cerrarConexion();

        } catch (ErrorException $e) {
            echo $e;
        }
        return true;
    }

    /**
     * $valores,$columna,$valor son variables que recibe para poder actualizar los datos del cliente
     */
    public function actualizarVentas($valores,$columna,$valor)
    {
        $sql = $this->updateQuery("ventas",$valores,$columna,$valor);
        try{
            $this->db->conectarDB();
            $this->db->ejecutar($sql);
            $this->db->cerrarConexion();
        }catch (ErrorException $e){
            echo  $e;
        }
        return true;
    }


    /**
     * $valores,$columna,$valor son variables que recibe para poder actualizar los datos del cliente
     */
    public function eliminarVenta($columna,$valor)
    {
        $sql = $this->eliminarQuery("ventas",$columna,$valor);
        try{
            $this->db->conectarDB();

            $this->db->ejecutar($sql);
//
            $this->db->cerrarConexion();
        }catch (ErrorException $e){
            echo  $e;
        }
        return true;
    }


    public function conseguirVenta(){

        $sql=$this->selectAllQuery("ventas");
        $this->db->conectarDB();
        $ventas =$this->db->consultar($sql);
        $this->db->cerrarConexion();
        return $ventas;
    }
    /*
     *
     */
    public function query($sql){

        $this->db->conectarDB();
        $venta = $this->db->consultar($sql);
        $this->db->cerrarConexion();

        return $venta;
    }
}