
<!doctype html>
<html lang="en">

<?php
include("vistas/head.php");
include_once('modelos/Auto.php');
//buscar autos
    $auto = new Auto("sistema_autos");
    $autos = $auto->conseguirAuto();


?>
<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h4 class="display-4">REGISTRAR VENTA</h4>

</div>

<div class="container" >
    <hr>

    <form method="Post" action="acciones.php">
        <input type="hidden" name="form" value="registrarV">
        <div class="form-group">
            <label >Auto</label>
            <select class="browser-default custom-select" name="id_auto">
                <option selected>Seleccione</option>
              <?php
               foreach($autos as $auto)
               {
                  echo '<option value=" '.$auto["id"].' ">'. $auto["nombre"] .' '.$auto["modelo"].'</option>';

                 }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="">Vendedor</label>
            <input type="text" class="form-control" id=""  name="vendedor"  >
        </div>
        <div class="form-group">
            <label for="">Cliente</label>
            <input type="text" class="form-control" id=""  name="cliente"  >
        </div>
        <div class="form-group">
            <label for="">DNI Cliente</label>
            <input type="text" class="form-control" id="" name="dni_cliente" >
        </div>
        <div class="form-group">
            <label for="">Sucursal</label>
            <input type="text" class="form-control" id="" name="local" >
        </div>
        <div class="form-group">
            <label for="">Cuotas</label>
            <input type="number" class="form-control" id="" name="cantidad_cuotas" >
        </div>
        <button type="submit" class="btn btn-outline-dark btn-block">Enviar</button>
    </form>
    <div class="links">
        <a href="index.php" class="font-italic text-decoration-none text-left"><- Registrar Auto</a>
        <a href="listado.php?id=ventas" class="font-italic text-decoration-none text-right" id="linkVentas">Ver Ventas -></a>
    </div>
    <?php
    include("vistas/footer.php");
    ?>



</div>
<?php
include("vistas/scripts.php");
?>
</body>
</html>
