<?php
//include('clases/ConexionDB.php');
//include_once('modelos/Auto.php');
//include_once('modelos/Ventas.php');
include_once('controlador/AdministradorController.php');
$type=null;
if(isset($_POST['form'])) {
    $type = $_POST['form'];
}
if ($type == 'registrarA') {

    $usuario = $_POST['usuario'];
    $nombre = $_POST['nombre'];
    $modelo = $_POST['modelo'];
    $marca = $_POST['marca'];
    $distribuidor = $_POST['distribuidor'];
    $origen =$_POST['origen'];
    $precio = $_POST['precio'];

    $array = array("nombre"=>$nombre,"usuario"=>$usuario,"modelo"=>$modelo,"marca"=>$marca,"distribuidor"=>$distribuidor,"origen"=>$origen,"precio"=>$precio);

    $registro = new AdministradorController($type);
    $registro->registrar($array);
}

if($type == "autoEdit")
{
    $hoy = date("Y-m-d H:i:s");
    $array = array("nombre"=>$_POST['nombre'],"usuario"=>$_POST['usuario'],"modelo"=>$_POST['modelo'],"marca"=>$_POST['marca'],
        "distribuidor"=>$_POST['distribuidor'],"origen"=>$_POST['origen'],"precio"=>$_POST['precio'],"fecha_ingreso"=>$_POST['fecha_ingreso'],"fecha_actualizacion"=>$hoy);

    $registro = new AdministradorController($type);
    $registro->modificar($array,$_POST['id'],"id");
}


if ($type == 'registrarV') {
    $ventas =array("vendedor"=> $_POST['vendedor'],
        "id_auto"=>$_POST['id_auto'],"local"=>$_POST['local'],"cliente"=>$_POST['cliente'],
        "dni_cliente"=>$_POST['dni_cliente'],"cantidad_cuotas"=>$_POST['cantidad_cuotas']);


    $registro = new AdministradorController($type);
    $registro->registrar($ventas);
}


if($type =="ventaEdit")
{
    $ventas =array("vendedor"=> $_POST['vendedor'], "local"=>$_POST['local'],"cliente"=>$_POST['cliente'],
        "dni_cliente"=>$_POST['dni_cliente'],"cantidad_cuotas"=>$_POST['cantidad_cuotas']);

    $registro = new AdministradorController($type);
    $registro->modificar($ventas,$_POST['id'],"id");
}
if(isset($_GET['tipo']))
{
    $type = $_GET['tipo'];
}


if($type =="borrarV" ||$type =="borrarA" )
{
    $id =$_GET['id'];

    $borrar = new  AdministradorController($type);
    $borrar->eliminarRegistro('id',$id);


}

?>