
<!doctype html>
<html lang="en">

<?php
include("vistas/head.php");
?>
<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h4 class="display-4">REGISTRAR AUTO</h4>
</div>

<div class="container" >
    <form method="Post" action="acciones.php">
        <input type="hidden" name="form" value="registrarA">
        <div class="form-group">
            <label for="nombres">Nombres</label>
            <input type="text" class="form-control" id="nombres" name="nombre" aria-describedby="" placeholder="Ingrese nombre">
        </div>
        <div class="form-group">
            <label for="">Marca</label>
            <input type="text" class="form-control" id=""  name="marca"  placeholder="Ingrese marca">
        </div>
        <div class="form-group">
            <label for="">Modelo</label>
            <input type="text" class="form-control" id=""  name="modelo"  placeholder="Ingrese modelo">
        </div>
        <div class="form-group">
            <label for="">Usuario</label>
            <input type="text" class="form-control" id="" name="usuario" placeholder="Ingrese usuario">
        </div>
        <div class="form-group">
            <label for="">Distribuidor</label>
            <input type="text" class="form-control" id="" name="distribuidor" placeholder="">
        </div>
        <div class="form-group">
            <label for="">Origen</label>
            <input type="text" class="form-control" id=""  name="origen"  placeholder="Enter origen">
        </div>
        <div class="form-group">
            <label for="">Precio</label>
            <input type="text" class="form-control" id=""  name="precio"  placeholder="Ingrese precio">
        </div>

        <button type="submit" class="btn btn-outline-dark btn-block">Enviar</button>
    </form>
    <div class="links">
        <a href="listado.php?id=autos" class="font-italic text-decoration-none text-left"><- Lista de Autos</a>
        <a href="listado.php?id=ventas" class="font-italic text-decoration-none text-right" id="linkVentas">ver Venta -></a>
    </div>
    <?php
    include("vistas/footer.php");
    ?>



</div>
<?php
include("vistas/scripts.php");
?>
</body>
</html>
