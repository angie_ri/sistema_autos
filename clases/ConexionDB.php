<?php 
class ConexionDB{

		private $conexionDB ;
		private $baseDeDatos;
	
	/**
	 * inicializa con el constructor ingresando la base de datos que va a trabajar
	 */
	function __construct($baseDeDatos){
		$this->baseDeDatos = $baseDeDatos;
		
		}
	/**
	 * Conecta con la base de datos 
	 */
	public function conectarDB(){

        //Declaramos las variables
        $db_name="$this->baseDeDatos";
        $dsn = 'mysql:dbname='.$db_name.';host=172.21.10.40';
        $username = "test";
        $password = "test123";

	    try{
        $conn = new PDO($dsn, $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->conexionDB = $conn;

	    } catch(PDOException $e){

            echo 'Falló la conexión: ' . $e->getMessage();
        }


        if($this->conexionDB){

				return $this->conexionDB;

			}

	}

	/*
	 *
	 */
    public function conseguirConexion(){
        return $this->conexionDB;
    }

	/**
	 * cierra la conexion con la BD
	 */
	public function cerrarConexion(){

		$this->conexionDB = null;

	}

	public  function  ejecutar($sql){
        try{
            $conectar =$this->conexionDB->prepare($sql);
            return $conectar->execute();
        } catch(PDOException $e){

        echo 'Falló la envío: ' . $e->getMessage();
        }

	}

	public  function  consultar($sql){

        $query=$this->conexionDB->query($sql,PDO::FETCH_ASSOC);

        return $query;
    }
}

?>